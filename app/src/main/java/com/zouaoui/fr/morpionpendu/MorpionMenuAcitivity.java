package com.zouaoui.fr.morpionpendu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by zouaoui on 01/06/2017.
 */

public class MorpionMenuAcitivity extends AppCompatActivity {
    LinearLayout morpion2Joueurs;
    LinearLayout morpionOrdinateur;
    LinearLayout back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morpion_menu);
        initialize();
        BindEventListener();
    }

    private void initialize() {
        morpion2Joueurs = (LinearLayout) findViewById(R.id.LinearLayoutTwoPlayers);
        morpionOrdinateur = (LinearLayout) findViewById(R.id.LinearLayoutOnePlayer);
        back = (LinearLayout) findViewById(R.id.LinearLayoutBack);
    }

    private void BindEventListener() {
        morpion2Joueurs.setOnClickListener(morpion2JoueursListener);
        morpionOrdinateur.setOnClickListener(morpionOrdinateurListener);
        back.setOnClickListener(backListener);
    }

    private View.OnClickListener morpion2JoueursListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MorpionMenuAcitivity.this, PlayersActivity.class);
            startActivity(appel);

        }
    };

    private View.OnClickListener morpionOrdinateurListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MorpionMenuAcitivity.this, MorpionOnePlayersActivity.class);
            startActivity(appel);

        }
    };

    private View.OnClickListener backListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            MorpionMenuAcitivity.this.finish();
        }
    };
}
