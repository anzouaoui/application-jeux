package com.zouaoui.fr.morpionpendu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

/**
 * Created by zouaoui on 02/06/2017.
 */

public class PlayersActivity extends AppCompatActivity {

    EditText firstPlayer;
    EditText secondPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players);
        initialize();
    }

    private void initialize() {
        firstPlayer = (EditText) findViewById(R.id.EditTextFirstPlayer);
        secondPlayer = (EditText) findViewById(R.id.EditTextSecondPlayer);
    }
}
