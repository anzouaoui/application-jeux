package com.zouaoui.fr.morpionpendu;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MenuActivity extends AppCompatActivity {

    LinearLayout morpion;
    LinearLayout pendu;
    LinearLayout exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initialize();
        BindEventListener();
    }
    private void initialize() {
        morpion = (LinearLayout) findViewById(R.id.LinearLayoutMorpion);
        pendu = (LinearLayout) findViewById(R.id.LinearLayoutPendu);
        exit = (LinearLayout) findViewById(R.id.LinearLayoutExit);
    }

    private void BindEventListener() {
        morpion.setOnClickListener(morpionListener);
        pendu.setOnClickListener(penduListener);
        exit.setOnClickListener(exitListener);
    }

    private View.OnClickListener morpionListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MenuActivity.this, MorpionMenuAcitivity.class);
            startActivity(appel);
        }
    };
    private View.OnClickListener penduListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MenuActivity.this, PenduActivity.class);
            startActivity(appel);
        }
    };
    private View.OnClickListener exitListener = new View.OnClickListener() {
        /**
         * Passer au jeux du morpion
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            System.exit(0);
        }
    };
}
